#JSONPlaceholder Example Android Project

[Api Documentation](https://jsonplaceholder.typicode.com/guide.html)

- [x] GET : /posts
- [x] GET : /comments?postId=$id
- [ ] POST : /posts
- [ ] PUT : /posts/$id
- [ ] DELETE : /posts/$id

Requirements 
- [x] Kotlin & AndroidX
- [x] MVVM Design Pattern
- [x] Retrofit & RxJava
- [x] Fragment (Navigation Jetpack)
- [x] Git

TODO : 
- [ ] Clean Code
- [ ] Unit Testing
- [X] Dagger / Koin (Dependency Injection)
- [ ] Dokumentation
