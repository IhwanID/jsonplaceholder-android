package id.ihwan.jsonplaceholder.network

import id.ihwan.jsonplaceholder.model.Comment
import id.ihwan.jsonplaceholder.model.Photo
import id.ihwan.jsonplaceholder.model.Post
import io.reactivex.Single
import retrofit2.http.*

interface ApiService {

    companion object {
        var instance: ApiService? = null

        fun instance() : ApiService{
            instance?.let {
                return it
            }?: return createInstance()
        }

        private fun createInstance(): ApiService{
            instance = Network.createService(ApiService::class.java)
            return instance as ApiService
        }

        fun removeInstance(){
            instance = null
        }
    }

    @GET("posts")
    fun getAllPost() : Single<List<Post>>

    @GET("photos")
    fun getAllPhotos() : Single<List<Photo>>

    @GET("posts/{id}")
    fun getPost(@Path("id") id: Int) : Single<Post>

    @PUT("posts/{id}")
    fun updatePost(@Path("id") id: Int?,
                   @Field("title") title: String?,
                   @Field("body") body: String?,
                   @Field("userId") userId: Int?) : Single<Post>

    @GET("comments")
    fun getAllComment(@Query("postId") id: Int) : Single<List<Comment>>

    @FormUrlEncoded
    @POST("posts")
    fun createPost(@Field("title") title: String?,
                   @Field("body") body: String?,
                   @Field("userId") userId: Int?) : Single<Post>

}