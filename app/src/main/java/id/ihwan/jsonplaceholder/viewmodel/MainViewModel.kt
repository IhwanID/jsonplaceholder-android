package id.ihwan.jsonplaceholder.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import id.ihwan.jsonplaceholder.model.Comment
import id.ihwan.jsonplaceholder.model.Post
import id.ihwan.jsonplaceholder.network.ApiService
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class MainViewModel: ViewModel() {

    private val _post = MutableLiveData<List<Post>>()

    val post: LiveData<List<Post>> get() =  _post

    private val _comment = MutableLiveData<List<Comment>>()

    val comment: LiveData<List<Comment>> get() =  _comment

    fun getAllPost(){
        ApiService.instance().getAllPost()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<List<Post>>{
                override fun onSuccess(t: List<Post>) {
                    _post.postValue(t)
                }

                override fun onSubscribe(d: Disposable) {

                }

                override fun onError(e: Throwable) {

                }

            })
    }

    fun getPostComment(id: Int){
        ApiService.instance().getAllComment(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<List<Comment>>{
                override fun onSuccess(t: List<Comment>) {
                    _comment.postValue(t)
                }

                override fun onSubscribe(d: Disposable) {

                }

                override fun onError(e: Throwable) {

                }


            })
    }
}