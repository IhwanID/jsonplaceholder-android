package id.ihwan.jsonplaceholder.view


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import id.ihwan.jsonplaceholder.R
import id.ihwan.jsonplaceholder.adapter.PostAdapter
import id.ihwan.jsonplaceholder.databinding.FragmentFeedBinding
import id.ihwan.jsonplaceholder.model.Post
import id.ihwan.jsonplaceholder.viewmodel.MainViewModel


class FeedFragment : Fragment() {

    private val adapter: PostAdapter by lazy {
        PostAdapter {
            goToDetail(it)
        }
    }

    private val viewModel by lazy { ViewModelProviders.of(this).get(MainViewModel::class.java) }

    private lateinit var binding: FragmentFeedBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_feed, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.feedRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = this@FeedFragment.adapter
            setHasFixedSize(true)
        }

        viewModel.getAllPost()

        viewModel.post.observe(this, Observer {
            adapter.loadData(it)
        })
    }

    private fun goToDetail(post : Post) {
        val action = FeedFragmentDirections.actionFeedFragmentToDetailFragment(post)
        findNavController().navigate(action)
    }


}
