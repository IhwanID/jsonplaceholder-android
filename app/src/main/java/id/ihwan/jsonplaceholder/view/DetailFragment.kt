package id.ihwan.jsonplaceholder.view


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import id.ihwan.jsonplaceholder.R
import id.ihwan.jsonplaceholder.adapter.CommentAdapter
import id.ihwan.jsonplaceholder.databinding.FragmentDetailBinding
import id.ihwan.jsonplaceholder.viewmodel.MainViewModel


class DetailFragment : Fragment() {

    private val adapter: CommentAdapter by lazy {
        CommentAdapter()
    }

    val args: DetailFragmentArgs by navArgs()

    private lateinit var binding: FragmentDetailBinding

    private val viewModel by lazy { ViewModelProviders.of(this).get(MainViewModel::class.java) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_detail, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("dataa", args.postArg.body)

        binding.model = args.postArg

        binding.commentRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = this@DetailFragment.adapter
            setHasFixedSize(true)
        }

        viewModel.getPostComment(args.postArg.id)

        viewModel.comment.observe(this, Observer {
            adapter.loadData(it)
        })
    }


}
