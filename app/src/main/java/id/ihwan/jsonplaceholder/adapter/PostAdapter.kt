package id.ihwan.jsonplaceholder.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.ihwan.jsonplaceholder.databinding.ItemPostBinding
import id.ihwan.jsonplaceholder.model.Post

class PostAdapter(val onClick: (Post) -> Unit) : BaseAdapter<PostAdapter.ViewHolder, Post>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemPostBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = models[position]
        holder.apply {
            bind(data)
            itemView.setOnClickListener { onClick(data) }
        }
    }

    class ViewHolder(private val binding: ItemPostBinding) : RecyclerView.ViewHolder(binding.root)  {
        fun bind(item: Post){
            binding.apply {
                model = item
                executePendingBindings()
            }
        }
    }
}