package id.ihwan.jsonplaceholder.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.ihwan.jsonplaceholder.databinding.ItemCommentBinding
import id.ihwan.jsonplaceholder.model.Comment


class CommentAdapter: BaseAdapter<CommentAdapter.ViewHolder, Comment>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemCommentBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = models[position]
        holder.bind(data)


    }

    class ViewHolder(private val binding: ItemCommentBinding) : RecyclerView.ViewHolder(binding.root)  {
        fun bind(item: Comment){
            binding.apply {
                model = item
                executePendingBindings()
            }
        }
    }
}